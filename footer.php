    <div role="contentinfo" class="uw-footer">

        <a href="<?php home_url('/'); ?>" class="footer-wordmark" title="<?php echo get_bloginfo('name'); ?>"><?php echo get_bloginfo('name'); ?></a>

        <!--<a href="http://www.washington.edu/boundless/"><h3 class="be-boundless">Be boundless</h3></a>-->

        <h4>Connect with us:</h4>

        <nav role="navigation" aria-label="social networking">
            <ul class="footer-social">
                <li><a class="facebook" href="https://www.facebook.com/UWglobalMH">Facebook</a></li>
                <li><a class="twitter" href="https://twitter.com/UWglobalMH">Twitter</a></li>
                <li><a class="instagram" href="https://www.instagram.com/globalhealthuw/">Instagram</a></li>
                <li><a class="youtube" title="YouTube Channel for Global Health" href="https://www.youtube.com/user/UWGlobalHealth">YouTube</a></li>
                <li><a class="youtube" title="YouTube Channel for Psychiatry and Behavioral Sciences" href="https://www.youtube.com/channel/UC7UnUAZQdvhSEpIVXQ2ok5w/videos">YouTube</a></li>
            </ul>
        </nav>

        <nav role="navigation" aria-label="footer links">
            <ul class="footer-links">
                <li><a href="http://globalhealth.washington.edu/contact">Contact Us</a></li>
                <li><a href="http://globalhealth.washington.edu/about-us/jobs">Jobs</a></li>
                <li><a href="http://globalhealth.washington.edu/events">Events</a></li>
                <li><a href="http://globalhealth.washington.edu/news">News</a></li>
                <li><a href="https://uwnetid-my.sharepoint.com/personal/deptgh_uw_edu/intranet/" target="_blank">Intranet</a></li>
                <li><a href="http://globalhealth.washington.edu/support-us">Donate</a></li>
            </ul>
        </nav>
        <nav role="navigation" aria-label="footer links uw">
            <ul class="footer-links">
                <li><a href="http://www.uw.edu/accessibility">Accessibility</a></li>
                <li><a href="http://uw.edu/home/siteinfo/form">Contact the UW</a></li>
                <li><a href="http://www.washington.edu/jobs">Jobs</a></li>
                <li><a href="http://www.washington.edu/safety">Campus Safety</a></li>
                <li><a href="http://myuw.washington.edu/">My UW</a></li>
                <li><a href="http://www.washington.edu/rules/wac">Rules Docket</a></li>
                <li><a href="http://www.washington.edu/online/privacy/">Privacy</a></li>
                <li><a href="http://www.washington.edu/online/terms/">Terms</a></li>
            </ul>
        </nav>

        <nav role="navigation" aria-label="footer links dghweb">
            <ul class="footer-links small">
                <li><a href="https://depts.washington.edu/dghweb/" target="_blank"><?php _e('Site managed by DGHweb', 'uwdgh'); ?></a></li>
                <li><?php dgh_site_login_link(); ?></li>
            </ul>
        </nav>

        <p>&copy; <?php echo date("Y"); ?> University of Washington  |  Seattle, WA</p>

    </div>

    </div><!-- #uw-container-inner -->
    </div><!-- #uw-container -->

<?php wp_footer(); ?>

</body>
</html>
