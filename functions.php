<?php
/**
 * DGH customizations, functions and definitions
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

/**
 * Replace uw-2014 scripts.
 *
 * Hooked to the wp_print_scripts action, with a late priority (100),
 * so that it is after the script was enqueued.
 */
function dgh_replace_script() {
  //replace UW's contact-card widget script with local script
  wp_dequeue_script('contact-card');
  wp_register_script('contact-card-fix', get_stylesheet_directory_uri() . '/assets/admin/js/widgets/uw.contact-widget.js', array('jquery'));
  wp_enqueue_script('contact-card-fix');
}
//add_action( 'wp_print_scripts', 'dgh_replace_script', 100 );

/**
 * Add the parent theme style.css.
 * This method is preferred over importing it into the style sheet
 */
function dgh_scripts_and_styles() {
  wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'dgh_scripts_and_styles' );
/**
 * add css for local overrides
 */
function dgh_enqueue_styles() {
  if (file_exists(get_stylesheet_directory() . '/overrides.css') ) {
    wp_enqueue_style( 'overrides', get_stylesheet_directory_uri() . '/overrides.css' );
  }
}
add_action( 'wp_enqueue_scripts', 'dgh_enqueue_styles', 11 );

/**
 * add js for addons
 */
function dgh_enqueue_addons_scripts() {
    //
  if (file_exists(get_stylesheet_directory() . '/assets/addons/scroll-up.js') ) {
      wp_register_script('uwgmh-scroll-up', get_stylesheet_directory_uri() . '/assets/addons/scroll-up.js', array('jquery'));
      wp_enqueue_script('uwgmh-scroll-up');
  }
}
add_action( 'wp_print_scripts', 'dgh_enqueue_addons_scripts', 100 );
/**
 * add css for addons
 */
function dgh_enqueue_addons_styles() {
  if (file_exists(get_stylesheet_directory() . '/assets/addons/scroll-up.css') ) {
    wp_enqueue_style( 'uwgmh-scroll-up', get_stylesheet_directory_uri() . '/assets/addons/scroll-up.css' );
  }
}
add_action( 'wp_enqueue_scripts', 'dgh_enqueue_addons_styles', 12 );

/* register theme header to default headers */
register_default_headers( array(
	'dgh' => array(
		'url'           => '%2$s/assets/header/Global_Health_2014.jpg',
		'thumbnail_url' => '%2$s/assets/header/Global_Health_2014-thumbnail.jpg',
		'description'   => __( 'DGH Map', 'dgh' )
	),
	'w3' => array(
		'url'           => '%2$s/assets/header/w3.jpg',
		'thumbnail_url' => '%2$s/assets/header/w3-thumbnail.jpg',
		'description'   => __( 'W Statue', 'w3' )
	),
	'w1' => array(
		'url'           => '%2$s/assets/header/w1.jpg',
		'thumbnail_url' => '%2$s/assets/header/w1-thumbnail.jpg',
		'description'   => __( 'W Fingers', 'w1' )
	),
	'blossoms' => array(
		'url'           => '%2$s/assets/header/blossoms.jpg',
		'thumbnail_url' => '%2$s/assets/header/blossoms-thumbnail.jpg',
		'description'   => __( 'Blossoms', 'blossoms' )
	),
	'fall-leaves' => array(
		'url'           => '%2$s/assets/header/fall-leaves.jpg',
		'thumbnail_url' => '%2$s/assets/header/fall-leaves-thumbnail.jpg',
		'description'   => __( 'Fall Leaves', 'fall-leaves' )
	),
	'ima' => array(
		'url'           => '%2$s/assets/header/ima.jpg',
		'thumbnail_url' => '%2$s/assets/header/ima-thumbnail.jpg',
		'description'   => __( 'IMA', 'ima' )
	),
) );

/* register thinstrip menu location */
function dgh_register_thinstrip_menu() {
  register_nav_menu('thinstrip-menu',__( 'Thinstrip' ));
}
add_action( 'init', 'dgh_register_thinstrip_menu' );

/* Builds the default thinstrip menu for DGH.
 * This menu is persistant, meaning if deleted via the admin interface
 * it will be recreated.
 *
 * Admins can override by creating a new menu and assigning it to the
 * Thinstrip menu location.
 */
function dgh_thinstrip_default_menu() {
  // Check if the menu exists
  $menu_name = "Global Health Audience Menu";
  $menu_exists = wp_get_nav_menu_object( $menu_name );
  // If it doesn't exist, let's create it.
  if( !$menu_exists){
    // Create the menu
    $menu_id = wp_create_nav_menu($menu_name);
    // Set up default menu items
    wp_update_nav_menu_item($menu_id, 0, array(
      'menu-item-title' =>  __('Academics'),
      'menu-item-url' => 'https://globalhealth.washington.edu/education-training/courses',
      'menu-item-attr-title' => 'Global Health Academics',
      'menu-item-status' => 'publish'));
    wp_update_nav_menu_item($menu_id, 0, array(
      'menu-item-title' =>  __('Our Work'),
      'menu-item-url' => 'https://globalhealth.washington.edu/interactive-map/projects',
      'menu-item-attr-title' => 'Our Work: Research, Service & Capacity Building',
      'menu-item-status' => 'publish'));
    wp_update_nav_menu_item($menu_id, 0, array(
      'menu-item-title' =>  __('Resource Center'),
      'menu-item-url' => 'https://globalhealth.washington.edu/connect/global-health-resource-center-ghrc',
      'menu-item-attr-title' => 'Global Health Resource Center',
      'menu-item-status' => 'publish'));

    // add the menu to the location just once
    $run_once = get_option('menu_check');
    if (!$run_once){
      //then get the menu object by its name
      $menu = get_term_by( 'name', $menu_name, 'nav_menu' );
      // set menu to the thinstrip location
      $locations = get_theme_mod('nav_menu_locations');
      $locations['thinstrip-menu'] = $menu->term_id;
      set_theme_mod( 'nav_menu_locations', $locations );
      // update the menu_check option to make sure this code only runs once
      update_option('menu_check', true);
    }
  }
}

/**
 * Override the pluggable function from the uw-2014 theme
 * @global type $post
 * @return type
 */
  function get_uw_breadcrumbs()
  {

    global $post;
    $ancestors = array_reverse( get_post_ancestors( $post->ID ) );
    $html = '<li><a href="' . home_url('/') . '" title="' . get_bloginfo('title') . '">Home</a></li>';
    if ( is_front_page() ) {
      $html .= '<li' . (is_front_page() ? ' class="current"' : '') . '><span>' . get_bloginfo('title') . '</span><li>';
    }

    if ( is_404() )
    {
        $html .=  '<li class="current"><span>Woof!</span>';
    } else

    if ( is_search() )
    {
        $html .=  '<li class="current"><span>Search results for ' . get_search_query() . '</span>';
    } else

    if ( is_author() )
    {
        $author = get_queried_object();
        $html .=  '<li class="current"><span> Author: '  . $author->display_name . '</span>';
    } else

    if ( get_queried_object_id() === (Int) get_option('page_for_posts')   ) {
        $html .=  '<li class="current"><span> '. get_the_title( get_queried_object_id() ) . ' </span>';
    }

    // If the current view is a post type other than page or attachment then the breadcrumbs will be taxonomies.
    if( is_category() || is_single() || is_post_type_archive() )
    {

      if ( is_post_type_archive() )
      {
        $posttype = get_post_type_object( get_post_type() );
        //$html .=  '<li class="current"><a href="'  . get_post_type_archive_link( $posttype->query_var ) .'" title="'. $posttype->labels->menu_name .'">'. $posttype->labels->menu_name  . '</a>';
        $html .=  '<li class="current"><span>'. $posttype->labels->menu_name  . '</span>';
      }

      if ( is_category() )
      {
        $category = get_category( get_query_var( 'cat' ) );
        //$html .=  '<li class="current"><a href="'  . get_category_link( $category->term_id ) .'" title="'. get_cat_name( $category->term_id ).'">'. get_cat_name($category->term_id ) . '</a>';
        $html .=  '<li class="current"><span>'. get_cat_name($category->term_id ) . '</span>';
      }

      if ( is_single() )
      {
        if ( has_category() )
        {
          $category = array_shift( get_the_category( $post->ID  ) ) ;
          $html .=  '<li><a href="'  . get_category_link( $category->term_id ) .'" title="'. get_cat_name( $category->term_id ).'">'. get_cat_name($category->term_id ) . '</a>';
        }
        if ( uw_is_custom_post_type() )
        {
          $posttype = get_post_type_object( get_post_type() );
          $archive_link = get_post_type_archive_link( $posttype->query_var );
          if (!empty($archive_link)) {
            $html .=  '<li><a href="'  . $archive_link .'" title="'. $posttype->labels->menu_name .'">'. $posttype->labels->menu_name  . '</a>';
          }
          else if (!empty($posttype->rewrite['slug'])){
            $html .=  '<li><a href="'  . site_url('/' . $posttype->rewrite['slug'] . '/') .'" title="'. $posttype->labels->menu_name .'">'. $posttype->labels->menu_name  . '</a>';
          }
        }
        $html .=  '<li class="current"><span>'. get_the_title( $post->ID ) . '</span>';
      }
    }

    // If the current view is a page then the breadcrumbs will be parent pages.
    else if ( is_page() )
    {

      if ( ! is_home() || ! is_front_page() )
        $ancestors[] = $post->ID;

      if ( ! is_front_page() )
      {
        foreach ( array_filter( $ancestors ) as $index=>$ancestor )
        {
          $class      = $index+1 == count($ancestors) ? ' class="current" ' : '';
          $page       = get_post( $ancestor );
          $url        = get_permalink( $page->ID );
          $title_attr = esc_attr( $page->post_title );
          if (!empty($class)){
            $html .= "<li $class><span>{$page->post_title}</span></li>";
          }
          else {
            $html .= "<li><a href=\"$url\" title=\"{$title_attr}\">{$page->post_title}</a></li>";
          }
        }
      }

    }

    return "<nav class='uw-breadcrumbs' role='navigation' aria-label='breadcrumbs'><ul>$html</ul></nav>";
  }


/**
 * Add custom metaboxes
 * @param $post
 */
function dgh_add_meta_boxes( $post ) {
  // Get the page template post meta
  $page_template = get_post_meta( $post->ID, '_wp_page_template', true );
  // If the current page uses our specific
  // template, then output our custom metabox
  if ( 'templates/template-slideshow.php' == $page_template ) {
    if (is_plugin_active('uw-slideshow/class.uw-slideshow.php') ) {
      add_meta_box(
        'dgh-slideshow-shortcode-metabox', // Metabox HTML ID attribute
        'UW Slideshow', // Metabox title
        'dgh_page_template_slideshow_shortcode_metabox', // callback name
        'page', // post type
        'side', // context (advanced, normal, or side)
        'core' // priority (high, core, default or low)
      );
    } else {
      add_action( 'admin_notices', 'dgh_admin_notice_slideshow_plugin__warning' );
    }
  }
}
// Make sure to use "_" instead of "-"
add_action( 'add_meta_boxes_page', 'dgh_add_meta_boxes' );

// Define the meta box form fields here
function dgh_page_template_slideshow_shortcode_metabox() {
  global $post;
  wp_nonce_field( 'slideshow_shortcode_save', 'slideshow_shortcode_nonce' );
  ?>
  <label for="slideshow_shortcode">Slideshow shortcode:</label><br>
  <input class="" type="text" name="slideshow_shortcode" id="slideshow_shortcode" value="<?php echo esc_attr( get_post_meta( $post->ID, 'slideshow_shortcode', true ) ); ?>" size="30" /><br><br>
  <input class="" type="checkbox" name="slideshow_no_title" id="slideshow_no_title" <?php if( get_post_meta( $post->ID, 'slideshow_no_title', true) == true ) { ?>checked="checked"<?php } ?> />&nbsp;<label for="slideshow_no_title">No Page Title</label><br>
  <p><a href="<?php echo admin_url( 'edit.php?post_type=slideshow', 'https' ); ?>">Go to Slideshows</a></p>
  <?php
}
// Save metabox POST values
function dgh_save_custom_post_meta() {
  global $post;
  // Sanitize/validate post meta here, before calling update_post_meta()
  if ( ! isset($_POST['slideshow_shortcode_nonce'])){
    return;
  }
  if ( ! wp_verify_nonce( $_POST['slideshow_shortcode_nonce'], 'slideshow_shortcode_save' ) ) {
    return;
  }

  $input = sanitize_text_field( $_POST['slideshow_shortcode'] );
  update_post_meta( $post->ID, 'slideshow_shortcode', $input );
  update_post_meta( $post->ID, 'slideshow_no_title', $_POST['slideshow_no_title'] );
}
add_action( 'publish_page', 'dgh_save_custom_post_meta' );
add_action( 'draft_page', 'dgh_save_custom_post_meta' );
add_action( 'future_page', 'dgh_save_custom_post_meta' );

//admin notice
function dgh_admin_notice_slideshow_plugin__warning() {
  ?>
  <div class="notice notice-warning is-dismissible">
      <p><?php _e( 'The <a href="https://github.com/uweb/uw-slideshow" target="_blank">uw-slideshow</a> plugin is required with the current template.', 'default' ); ?></p>
  </div>
  <?php
}

/**
* Login/Logout link
*/
function dgh_site_login_link() {
  if (is_user_logged_in()) {
    $current_user = wp_get_current_user();
    echo '<a href="' . wp_logout_url(get_permalink()) . '" title="' . __('Log out', 'default') . '">' . __('Log out', 'default') . ' <em>' . $current_user->user_login . '</em></a>';
  } else {
    echo '<a href="' . wp_login_url(get_permalink()) . '" title="' . __('Log in', 'default') . '">' . __('Log in', 'default') . '</a>';
  }
}
