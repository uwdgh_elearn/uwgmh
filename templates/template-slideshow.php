<?php
/**
 * Template Name: UW Slideshow (requires <a href="https://github.com/uweb/uw-slideshow" target="_blank">uw-slideshow</a> plugin)
 */
?>

<?php get_header(); 
  $slideshow = esc_attr( get_post_meta($post->ID, "slideshow_shortcode", true) ); 
  $no_title =  get_post_meta($post->ID, "slideshow_no_title", true);
  $sidebar = get_post_meta($post->ID, "sidebar");
?>

<div class="mobile-menu-wrapper">
  <?php get_template_part( 'menu', 'mobile' ); ?>
</div>

<div class="uw-hero-image hero-height">
  <div class="uw-hero-slideshow">
    <?php echo do_shortcode( $slideshow ); ?>
  </div>
  <?php if ($no_title == false) { ?>
    <div id="hero-bg">
      <div id="hero-container" class="container">
        <h1 class="uw-site-title"><?php the_title(); ?></h1>
        <span class="udub-slant"><span></span></span>
      </div>
    </div>
  <?php } ?>
</div>


<div class="container uw-body">

  <div class="row">

    <div class="col-md-<?php echo (($sidebar[0]!="on") ? "8" : "12" ) ?> uw-content" role='main'>

      <?php uw_site_title(); ?>
      <?php get_template_part( 'breadcrumbs' ); ?>

      <div id='main_content' class="uw-body-copy" tabindex="-1">


        <?php
          // Start the Loop.
          while ( have_posts() ) : the_post();

            //the_content();
            get_template_part( 'content', 'page-noheader' );

            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) {
              comments_template();
            }

          endwhile;
        ?>

      </div>

    </div>

    <div id="sidebar"><?php 
      if($sidebar[0]!="on"){
        get_sidebar();
      }
    ?></div>

  </div>

</div>

<?php get_footer(); ?>
